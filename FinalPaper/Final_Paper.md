# Quantifying Fishing Pressure through Fish Morphometrics Identified by Deep Neural Networks

Abhiraj Vinnakota, Zhiwei Calvin Dong, Jingjing Shi, Yuqiang Zhang, Steve Canty, Steven Box

## Abstract

Fisheries management aims to manage exploited fish populations and set catch limits for specific fishing grounds in order to keep fisheries sustainable. Therefore, evaluating catches is important for fishery management. Traditional methods applied to this field include genetic analysis, microchemistry, and morphometrics. These methods are not very accurate especially when the dataset is complex and costly due to too much human effort, advanced image detection and classification techniques are being considered. This project implemented state-of-arts deep learning models to predict fish species, countries, and fishing grounds of origin. Deep learning models trained with photographic images of fish achieved much higher accuracy than traditional methods. The object detection model achieved 84.0% accuracy. The four classification models achieved 86.7% for classification of fish species, 97.6% for classification of countries of yellowtail snapper, 98.8% for classification of countries of white grunt, and 97.4% for classification of fishing grounds of yellowtail snapper in Honduras. A hierarchical model structure consisted of one object detection model and four classification models were developed to make the prediction process logical. The hierarchical model structure was deployed on an HTML application so that users can easily leverage this tool in just a few clicks.


## 1. Introduction

Fisheries management includes monitoring exploited fish populations, estimating maximum sustainable yield, and setting catch limits. To effienctly manage fisheries, tracking key metrics such as the number of people fishing, the quantity of fish that is being caught, the different species that are being caught as well as the locations of fishing activity are of vital importance. 

Though the mechanism for tracking almost all these metrics can be established at the point of entry, the collection of location data has lagged. This is because existing methods including physical patrols, port side controls, application of GPS systems, and are both expensive and inefficient. Moreover, fishermen are rather apprehensive of giving their exact fishing locations because they think tracking people involves privacy issues. There is a dire need for alternative ways to collect location data efficiently and enable better fisheries management. 

One recent study by Canty et al., utilizes genetic analysis, microchemistry, and morphometrics to identify fish origins[1].  They collected fish (Yellowtail Snapper) from three different fishing grounds, separated by a minimum of 5 km and a maximum of 60 km. They found out that morphometrics analysis is cost-effective and can achieve 80% accuracy when assigning fishing grounds in 5-10 km geographical ranges. Also, they found that there are distinct shapes in different fishing grounds of yellowtail snapper, which suggests that it is possible to automate the morphometric analysis using digital images of sample fish to get more accurate results and to incorporate these results into a user-friendly system that could be easily interpreted by other stakeholders. A more recent study, conducted by Nunez-Vallecillo et al., conducted similar morphometric analyses on the land snapper and white grunt. They found significant differences in the body shape in both species across four sites located along the Honduran northshore [2].

<img src="./truss_points.png" width="720" >

*Figure 1 The ten morphometric truss points overlaid on a yellowtail snapper [1]*

However, the Canty et al. (2018) study relied on the manual collection of data using calipers to measure distances between fixed truss points (Figure 1). The Nunez-Vallecillo et al., 2021 study digitized the placement of landmarks, both techniques which require significant time in placing landmarks and can be prone to human error.  Therefore, with the advances in image recognition techniques can these steps be automated and not only reduce error but increase the speed and accuracy of morphometric analyses in assigning individuals to their site of origin.

In this paper, a variety of computer vision algorithms will be applied to identify fishing grounds based on fish morphometric using photographic images. The main goal of this project is to automate the classification of images of fish to the correct species, and subsequently assign them to their country and fishing ground of origin.

## 2. Methods

In this paper, a variety of computer vision algorithms will be applied to identify fishing grounds based on fish morphometric using photographic images. The main goal of this project is to identify the species, country and fishing ground of the fish in an image, therefore, an object detection model will be utilized to identify the fish in an image followed by image classification models to assign species and locations of the fish.

### 2.1 Geographic focus of the work

In this paper, the data was provided by the Smithsonian Marine Station  and the Fish Forever Program at Rare, which includes photographic images of three species of fish, Yellowtail snapper, white grunt and lane snapper, from different fishing grounds in Honduras and Belize.

Table 1 shows the distribution of three types of fishes in the two countries. 

*Table 1 Distributions of Fish in Two Countries*

|          | Yellowtail Snapper | White Grunt | Lane Snapper |
|----------|--------------|-------------|----------------|
| Honduras | 219            | 52          | -           |
| Belize   | 402          | 45          | 408           |

Yellowtail snapper has a more evenly-distributed image in different fishing grounds, and white grunt species don't have enough data for fishing ground classification, so we will only classify fishing grounds for yellowtail snapper. The original image counts in each fishing ground of yellowtail snapper summarized in table 2, which suggests a well-distributed dataset.

*Table 2 Distribution of Yellowtail Snapper in Original Fishing Ground*

|Country| Fishing Grounds | Number of Images|
|:---------------:|:---------------:|:--------------------:|
|Belize|   Grunt Patch   |          103        |
|Belize|   Harvest Caye  |          176         |
|Belize|             Jack Bank         |123|
|Honduras|    Banco Tino   |          134        |
|Honduras|  Seco en Medio  |          85          |



*Table 3. Distribution of Yellowtail Snapper in Collapsed Fishing Ground*

|Country|             Fishing Ground            |Number of Images|
|:--------------:|:-------------------------------------:|:----------------:|
|Belize| Grunt Patch + Harvest Caye + Jack Bank |        402       |
|Honduras|               Banco Tino              |        134       |
|Honduras|             Seco en Medio             |        85        |

<img src="./gps.png" width="720" >

*Figure 2. Geographic location of fishing grounds.*

### 2.2 Species included in the study

As shown in Table 1, there are three species included in this paper; yellowtail snapper (*Ocyurus chrysurus*), white grunt (*Haemulon plumierii*), and lane snapper (*Lutjanus synagris*), and the first step in the classification models is to identify the species of a fish. Since there are much fewer images with white grunt, we will create a balanced sample from the other two species based on the total amount of images of white grunt.

### 2.3 Assisted v.s. Unassisted images

It should also be noted that two different versions of photos were taken for each fish. One is ‘assisted’ images taken with a color panel that is used for color calibration, while another is the ‘unassisted’ images only taken with the fish. Since some of the ‘assisted’ images were wrongly labeled as 'unassisted', to maintain the data integrity, the color panels in those images were manually cropped out. Considering the physical distances between the three Belizean fishing grounds (Jack Bank, Grunt Patch, and Harvest Caye) were fewer than 2 miles apart(Figure 3), we aggregated them into one category.

Considering the background noise issue which we explain in section 2.6 is much challenging in 'assisted' images, in this project, we only utilized ‘unassisted’ images to train the classification model; As to object detection models, based on our exploration (section 2.6), their performance was not severely affected by background noise of training samples but highly depended on training sample size. Therefore, to take full advantage of raw images, both ‘unassisted’ and ‘assisted’ images were used to train object detection models. 

 ### 2.4 Hierarchical modeling
 
 We will use a hierarchical modeling structure to achieve our goals. First the object detection model will identify the availability of the fish in the image, then the specific species will be classified. Depend on the species result, we will continue to classify the country of white grunt and yellowtail snapper. With yellowtail snapper, there is another layer of fishing ground classification after country level. The hierarchical structure of the models are shown below in Figure 3.

<img src="./Hierarchical_model.png" width="720" >

*Figure 3. Hierarchical model structure*

### 2.5 Techniques

Object detection is a computer vision technique that identifies and locates objects in an image. Usually, a selective search will be applied first to generate a region of interest proposals, and then the images will be fed into Convolutional Neural Network (CNN) of choice and get bounding box regression while getting classification results at the same time. 

Model deployment changing programmer-oriented model outputs to human-legible text helps non-technical people better understand what the black-box models generate. For this project, we deploy an HTML application to our end-users so that they can utilize the models with several easy clicks. Flask [2] is one of the most popular engines in Python for model deployment. It is a lightweight Web Server Gateway Interface(WSGI) as well as a micro-framework written in Python. The modules included in Flask help developers build web applications easily. 


#### 2.5.1. YOLO

You Only Look Once (YOLO) model [3] is a simplified version of regions with Recurrent Convolutional Neural Networks (R-CNN), which only contains one stage of training where the confidence is trained to predict the intersection of the union to any ground truth from any class. YOLO will be applied first to get the bounding box location of the fish. YOLO applies a single neural network to the full image; this network divides the images into regions and predicts bounding boxes and probabilities for each region. YOLO is suitable for this project because fish in all images with the same scales, and YOLO is accurate and fast when handling same-scale objects. 

YOLO is a unified, real-time object detection model. During the training process, the confidence score is trained to predict the intersection of union (IoU) to any ground truths from any classes, and take an image as input while output a grid of size B*5*C (B is the number of anchor boxes; C is the number of classes.). The confidence score is defined as for each bounding box, predict the confidence (IoU) of the predicted box (offset shifted/resized) with any bounding box in the image.
<img src="./yolo.png" width="720" >

*Figure 4 Model Architecture of YOLO-V5 [3]*

#### 2.5.2 ResNet and Inception-V3

To achieve the main goal of this paper, image classification is another computer vision technology applied. Image classification problems in this paper include binary classification such as identifying which country the fish comes from and multiclass classification such as identifying specific fishing grounds. Considering limited training samples and computing resources, we selected two popular pre-trained models ResNet-34 [4] and Inception-V3 [5], as original models and trained on our training set. 

ResNet is a chain-like model (Figure 5) [4]. The core innovation of ResNet is residual learning, which is easier to optimize and gains accuracy from deeper networks. It makes shortcuts for original activations every few stacked layers. ResNet significantly increased CNN models' capacity by small architecture engineering therefore it doesn't take dramatically high computational costs so that becomes one of the most popular CNN models in recent years. ResNet-34 will be used in this paper since it allows for deeper networks that still converge.

<img src="./resnet.png" width="180" >

*Figure 5 ResNet34 Model Architecture (different colors represent different output channels)[4]*

The inception model is a multi-branched model that provides more connections between different layers (Figure 1.6) [5]. The general idea of Inception models is to increase the width of the model so that the representation power will not decrease fastly especially in starting layers. In Inception-V3, smaller filters are used in inception modules. This is the most obvious change compared with the original version of the Inception model.

<img src="./inception.png" width="360" >

*Figure 6 Inception-V3 Model Architecture*

#### 2.5.3 Flask

Flask was used in model deployment [2]. The frontend includes the User Interface (UI), the Data Encoder, and the Data Decoder. The backend includes the Data Preprocessor, the Hierarchical Model, and the Prediction Postprocessor. Users can upload multiple images from the UI. Then those images are encoded and sent to the backend. In the backend, data is preprocessed as valid inputs then sent to the hierarchical model. Predictions generated by the model are then sent to a post-processor. Finally, outputs of the backend will be sent back to the frontend. The frontend decodes the outputs then presents the final results in the UI. 

 
<img src="./Model_deployment_framework.png" width="720" >

*Figure 7 Model Deployment Framework*

### 2.6 Data Preprocess in Classification Models

The photographic images were taken at different fishing grounds at the time the fish was caught, therefore, camera settings and environmental factors were not consistent throughout all images. To achieve more accurate CNN models that only take features from fish itself into account, several data preprocessing methods were applied.
 
Grayscale was applied to eliminate the effects if the images were taken from different daylight or angles, even though this will ignore potential true color diversity. Images from Belize were brighter than those from Honduras. In order to ensure models focus on the shape features, brightness was adjusted to the same scale (Figure 7,8). 


<img src="./color_brightness.png" width="720" >

*Figure 7. Color and brightness difference of images from two countries*

<img src="./grayscale.png" width="720" >

*Figure 8. Grayscale and brightness adjust eliminate the color and brightness difference*

Center cropping was applied to raw images to get rid of color panels at edges and corners since those objects would introduce noises for CNN models.

<img src="./center_crop.png" width="720" >

*Figure 9. Center cropping captures only features from fishes*

There were also some remaining issues after preprocessing such as the stripes of the background. Although CNN models have strong feature extraction modules that are able to efficiently extract useful information from raw images by implementing different convolutional kernels and pooling schema, the raw images were of two different backgrounds, and the images with stripes in backgrounds disturb CNN models in feature extraction phase. The results from tested models with shallow convolutional layers were shown below:

<img src="./background.png" width="720" >

*Figure 10. Left: Before feature extraction. Right: After feature extraction*

The models can extract the contour of fish as well as detailed information such as mouth, eyes, and so on. However, the stripes in the background can still be observed. This issue was carefully handled during training processes by leveraging deeper and wider convolutional layers.

### 2.7 Model Settings

For both object detection and classification models, we inplemented some data augmentation techniques as well as popular initial settings for the model hyperparameters. 

#### 2.7.1 Object Detection Models

*Table 4. YOLO-V5 Model Parameters*

|           | Resize    | Flip       | Rotation    | Saturation | Brightness | Exposure |
|-----------|-----------|------------|-------------|------------|------------|----------|
| Parameter | [416,416] | Rate = 0.5 | ± 15 degree | ± 25%      | ± 25%      | ± 25%    |

#### 2.7.2 Classification Models

First, the two classification models ResNet-34 and Inception-V3 were implemented as initial attempts to validate the usage of deep learning methods in fish image classification. The original dataset was randomly split into 80% training and 20% validation, and data augmentation approaches were only implemented to the training set. Considering the issue of imbalanced labels, we also matched the sample size among different groups. As for model evaluation, both models were trained on the training set and validated on the validation set. All raw images were included in the test set. A more detailed summary of the model setting is shown in the table below:

*Table 5. Model Parameters in Classification Models*

|                                   |                     | ResNet-34                         | Inception-V3                      |
|-----------------------------------|---------------------|-----------------------------------|-----------------------------------|
| Data Augmentation/ Transformation | Resize              |             [100, 150]            |             [299, 299]            |
|                                   | Center Crop         |             [70, 150]             |                 -                 |
|                                   | Random Crop         |             [50, 150]             |             [150, 200]            |
|                                   | Horizontal Flip     |             Rate = 0.5            |             Rate = 0.5            |
|                                   | Brighness Adjust    |                0.1                |                0.1                |
| Model Setting                     | # of Epoch          |                100                |                100                |
|                                   | Learning rate       |                0.1                |                0.1                |
|                                   | Learning rate decay |       0.1 at [50, 75] epochs      |       0.1 at [50, 75] epochs      |
| Loss Function                     | Loss function       |           Cross Entropy           |           Cross Entropy           |
| Optimizer                         | Optimization        | Stochastic Gradient Descent (SGD) | Stochastic Gradient Descent (SGD) |
|                                   | Momentum            |                0.9                |                0.9                |
|                                   | Regularization      |                 L2                |                 L2                |
|                                   | Reg. strength       |               0.0002              |               0.0002              |


As mentioned in the previous section, grayscale can be applied to eliminate the effects of color difference, but it will ignore potential true color diversity, so to further evaluate the color feature, RGB images and grey-scaled images were used as two different sets of inputs. 


## 3 Results

### 3.1 Object Detection Models

Object detection is applied to first identify if the image has a fish or not and more importantly locate the fish in the given image, by drawing a bounding box around it.
 
A total of 939 images were used in this method, which contains 457 images of yellowtail snapper and 482 images of lane snapper. Both ‘assisted’ and ‘unassisted’ images were included as having fish of different sizes in various positions along with other noise elements serves to be useful for training this model. The bounding boxes around the fish were manually drawn for each image. YOLO-V3 was used for this task, and the model was trained for 100 epochs with the default parameters (Table 4).




Figure 11 showed examples of the augmented images during training, the training set contains all variations in images, which makes the model more robust and be able to handle messy images effectively.

<img src="./model_3.png" width="720" >

*Figure 11. Sample augmented images used for training*

As for model performance, the model achieved the highest mean average precision (mAP) of 0.8396 in the 94th epoch. Then the model was tested on a hold-out set of around 50+ fish images. The model successfully identified all fish correctly and even drew bounding boxes pretty accurately as seen below in Figure 12. 

<img src="./model_4.png" width="720" >

*Figure 12. Left: An assisted image of a lane snapper with the predicted bonding box. Right: An unassisted image of the yellowtail snapper with the predicted bonding box.*

### 3.2 Classification Models

We developed four classfication models: species classification model, country classification model for yellowtail snapper, country classification model for white grunt and fishing ground classification model for yellowtail snapper in Honduras. We also explored patterns of misclassified images in order to improve the model performances.

#### 3.2.1 Classification of Species

The fish species included in this project are yellowtail snapper, white grunt and lane snapper. Ignoring the country and fishing ground, the prediction model needs to classify input images as one of three species. Due to the similarity in feature spaces and limited training samples, the Inception-V3 was selected. The prediction accuracy varied by species, greatest accuracies were observed in white grunts and yellowtail snappers, respectively (Table 6). 

*Table 6 Prediction Accuracy of Species*

|        |                | Predicted    |             |                |          |
|--------|----------------|--------------|-------------|----------------|----------|
|        |                | Yellowtail Snapper | White Grunt | Lane Snapper | Accuracy |
| Actual | Yellowtail Snapper   | 548           | 18          | 9            | 96.8%    |
|        | White Grunt    | 0            | 85          | 0              | 100.0%   |
|        | Lane Snapper | 1         | 118         | 272              | 69.6%    |

The overall prediction accuracy is 86.7%. However, the prediction accuracy of the lane snapper is 69.6%. Since Lane Snapper only has data from Belize, the potential bias may exist on the country level. We also tested this assumption by only using images from Belize. The prediction results are shown below:


*Table 7 Prediction Accuracy of Species in Belize*

|        |                | Predicted    |             |                |          |
|--------|----------------|--------------|-------------|----------------|----------|
|        |                | Yellowtail Snapper | White Grunt | Lane Snapper | Accuracy |
| Actual | Yellowtail Snapper   | 412         | 0         | 2             | 94.6%    |
|        | White Grunt    | 1            | 38          | 0              | 97.4%    |
|        | Lane Snapper | 11            | 10          | 370            | 99.5%    |

The overall prediction accuracy in Belize is 97.1%. All three categories are beyond 94%. These results justify our assumption.

#### 3.2.4 Classification of Countries

Since all images of lane snapper are from Belize, we developed country classification models for yellowtail snapper and white grunt only. Based on the previous research [1], the classification of countries is much easier since fish in different countries tend to have different morphometrics. Both ResNet-34 and Inception-V3 had 95% or higher prediction accuracy for both species. Since Inception-V3 took much higher computational costs (244 minutes on average for training) than ResNet-34 (57 minutes on average for training), ResNet-34 was sufficient for this classification task. The prediction accuracy is shown below:

*Table 8 Classification Accuracy of Countries from ResNet-34*

|               | Overall Accuracy |
|---------------|-----------------:|
| Yellow Snapper| 97.6%            |
| White Grunt   | 98.8%            |


The prediction accuracy of yellowtail snapper and white grunt is 97.6% and 98.8%. For the yellowtail snapper, there were two misclassified images in Belize. Both of them are with stripe backgrounds (Figure 14). As to Honduras, 10 images were misclassified. 9 of them had stripe backgrounds. For white grunt, there was only one image from Belize misclassified as Honduras (Figure 15). Therefore, as per the previous assumption, background noise is a challenge for CNN models in this project.

<img src="./model_1.png" width="720" >

*Figure 14. Top: Examples of misclassified images of yellowtail snapper in Belize. Bottom: Examples of misclassified images of yellowtail snapper  in Honduras*

<img src="./15.png" width="300" >

*Figure 15 Examples of misclassified images of white grunt*  

#### 3.2.5 Classification of Fishing Grounds

Based on the results of the data description, images of lane snapper and white grunt are biased or limited for fishing grounds classification so we mainly focus on yellowtail snapper in this section. Classifying fishing grounds is more complex than classifying countries because a deeper and wider prediction model is needed to explore more information from the identical raw data. Since both ResNet-34 and Inception-V3 showed promising results on the classification of countries, we started with two models trained on the original data. The overall prediction accuracy for ResNet-34 and Inception-V3 is 31.9% and 63.5%, respectively. The detailed accuracy of the Inception-V3 model for each fishing ground is shown in Table 9.

*Table 9 Prediction accuracy of classification of fishing grounds (Inception-V3)*

|        |               | Predicts   |             |              |           |               |          |
|--------|---------------|------------|-------------|--------------|-----------|---------------|----------|
|        |               | Banco Tino | Grunt Patch | Harvest Caye | Jack Bank | Seco en Medio | Accuracy |
| Actual | Banco Tino    | 67         | 7           | 27           | 30        | 3             | 50.0%    |
|        | Grunt Patch   | 0          | 11          | 81           | 10        | 1             | 10.7%    |
|        | Harvest Caye  | 0          | 2           | 167          | 4         | 3             | 94.9%    |
|        | Jack Bank     | 0          | 0           | 57           | 66        | 0             | 53.7%    |
|        | Seco en Medio | 0          | 0           | 0            | 2         | 83            | 97.7%    |

Based on Table 9, most images from the three Belizean fishing grounds (Grunt Patch, Harvest Caye, and Jack Bank) were classified as Harvest Caye. This is the biggest issue for this model. As we discussed in the data description section,  these three fishing grounds were geographically close to each other, they are prone to have similar features concerning shape, colors, and so on, therefore the prediction models possibly misclassified them as a single fishing ground.

Based on the assumption from geographical view, the three Belizean fishing grounds were integrated as one category. Then we trained the model on the new data and had the prediction accuracy significantly improved to 66.0% and 97.4% for ResNet-34 and Inception-V3, respectively. The detailed accuracy of the Inception-V3 model for each fishing ground is shown below:

*Table 10 Prediction accuracy of classification of regrouped fishing grounds (Inception-V3)*

|        |                                        | Predicts   |                                        |                |          |
|--------|----------------------------------------|------------|----------------------------------------|----------------|----------|
|        |                                        | Banco Tino | Harvest Caye + Jack Bank + Grunt Patch | Seco en Medio  | Accuracy |
| Actual | Banco Tino                             | 132        | 2                                      | 0              | 99.1%    |
|        | Harvest Caye + Jack Bank + Grunt Patch | 2          | 398                                    | 2              | 99.4%    |
|        | Seco en Medio                          | 10         | 1                                      | 74             | 87.1%    |

From the above table, the prediction model performed very well in the first two fishing grounds. As to Seco en Medio, images from this fishing ground were not as many as the other two. Data imbalance issues might lead to misclassification.

The misclassified images are shown below. The background noises were still the biggest issue. Almost all misclassified images were not white-background.

<img src="./16.png" width="720" >

*Figure 16. Example of misclassified images in fishing grounds*

Since the previous results inferred an assumption that prediction models were mainly disturbed by Belizean fishing grounds, if Inception-V3 and ResNet-34 could perform well within two Honduran fishing grounds and fail within three Belizean fishing grounds, this assumption would be justified. Based on this idea, four models (two Inception-V3 and two ResNet-34 for Honduran and Belize fishing grounds respectively) were developed to assess the assumption. The results are shown in the following tables.

*Table 11 Prediction accuracy of classification of Honduran fishing grounds (ResNet-34)*


|        |               | Predicts   |               |           |
|--------|---------------|------------|---------------|-----------|
|        |               | Banco Tino | Seco en Medio | Accuracy* |
| Actual |   Banco Tino  |     129    |       5       |   96.3%   |
|        | Seco en Medio |     10     |       75      |   88.2%   |

*The overall accuracy is 93.2%*

*Table 12 Prediction accuracy of classification of Honduran fishing grounds (Inception-V3)*

|        |               | Predicts   |               |           |
|--------|---------------|------------|---------------|-----------|
|        |               | Banco Tino | Seco en Medio | Accuracy* |
| Actual |   Banco Tino  |     134    |       0       |   100.0%  |
|        | Seco en Medio |     13     |       72      |   84.7%   |

*The overall accuracy is 94.1%*

*Table 13 Prediction accuracy of classification of Belizean fishing grounds*

|              | Inception-v3 | ResNet-34 |
|:------------:|:------------:|:---------:|
|  Grunt Patch |    57.3%     |   30.5%   |
| Harvest Caye |    41.5%     |   49.8%   |
|   Jack Bank  |     1.6%     |   6.0%    |

The assumption of models being disturbed by Belizean fishing grounds was justified by the results shown in the above tables. ResNet-34 and Inception-V3 performed well in Honduran fishing grounds while Inception-V3 only achieved 33.3% accuracy which was equivalent to random guessing. The accuracy of ResNet-34 is 30.1% that was lower than Inception-V3.

Interestingly, when dealing with less complicated classification tasks, ResNet-34 had more competitive performances with Inception-V3[5]. This was also seen in the classification of countries.


## 5. Model Deployment 

The model deployment is developed by the Flask engine. As described in Figure 7, users upload their images onto the server, and the Flask app runs the results through the hierarchical structure then presents detailed results. More specifically, the model first detects the existence of fish in the image using YOLO-V5, and if a fish is detected, the Inception-V3 model gives the specific species of the fish. Due to the data limitation, for lane snapper, the model only classifies images on the species level; for white grunt, the country of origin will be identified; and for yellowtail snapper, in addition to predicting the country, the specific fishing ground inside Honduras is predicted as well.

This process may take seconds depending on the number of uploaded images and computing sources. The examples and results shown below are some uploaded images (with true labels) and the prediction results provided by the model. Based on the true labels in Figure 17 and results in Figure 18, the model successfully detected fish and predicted species, countries, and fishing grounds. We mark the results by using 0.2 and 0.7 as thresholds of the confidence scores of the object detection model and the classification models respectively. In this way, users can get more insights about how confident the model is for the results. 
  
<img src="./17.png" width="720" >

*Figure 17. Examples of uploading images. Top row: 0, Fish, Lane Snapper; 1, Fish, Lane Snapper; 2, Fish, White Grunt, Belize; 3, Fish, Yellowtail Snapper, Belize. Bottom row: 4, Fish, White Grunt, Belize; 5, Fish, White Grunt, Honduras; 6, Fish, Yellowtail Snapper, Honduras, Seco en Medio; 7, Not fish.*


<img src="./18.png" width="500" >

*Figure 18. Results of the hierarchical prediction model. \* means the confidence score is below 0.7,a nd this classification should not be accepted.*

## 6. Conclusion and Discussion

Based on the results shown in previous sections, our hierarchical model consists of one object detection model and four classification models that can successfully identify fish and predict fish species, countries as well as fishing grounds. All five models have high accuracy. The object detection model achieves 84.0% accuracy. The four classification models achieve 86.7% for classification of fish species, 97.6% for classification of countries of yellowtail snapper, 98.8% for classification of countries of white grunt, and 97.4% for classification of fishing grounds of yellowtail snapper in Honduras. Once more fish images are brought in, the model will be more accurate and generalized, which benefits fishery management in the future. In addition, we deploy the model to an HTML application that could be easily connected to a remote server. We also find deep neural networks with deeper and wider layers, such as Inception-V3, have better performance than those with shallower layers and chain-like structures when dealing with complex dataset. 

However, there are some limitations we would like to discuss: 1) Due to limited training samples, the four classification models may have high variance. This could be solved by having more training data; 2) The issue of imbalanced labels affects the performance of classification models especially for country prediction of white grunt and fishing grounds prediction of yellowtail snapper in Honduras. It may be helpful if future training processes use other state-of-arts techniques, for example using focal loss as the loss function; 3) As we pointed out several times in previous sections, the background noise of images is the biggest challenge during our training processes. Although the four classification models perform well, we anticipate the background noise may be much more challenging when training with a broader dataset; 4) Our classification models cannot distinguish three Belizean fishing grounds. Based on our research, the main reason probably is, from the view of pysical distance, they are so close to each other that fish don't have enough detectably different features. While, since we only have about 500 training samples, we cannot imagine whether this issue could be solved by having more training samples. Theoretically, larger training sample size provide more general feature distributions so that make more accurate dicision boundaries.

## References 

[1] Canty, Steven WJ, Nathan K. Truelove, Richard F. Preziosi, Simon Chenery, Matthew AS Horstwood, and Stephen J. Box. "Evaluating tools for the spatial management of fisheries." Journal of Applied Ecology 55, no. 6 (2018): 2997-3004.

[2] Welcome to flask. (n.d.). Retrieved March 28, 2021, from https://flask.palletsprojects.com/en/1.1.x/

[3] Redmon, Joseph, Santosh Divvala, Ross Girshick, and Ali Farhadi. "You only look once: Unified, real-time object detection." In Proceedings of the IEEE conference on computer vision and pattern recognition, pp. 779-788. 2016.

[4] He, Kaiming, Xiangyu Zhang, Shaoqing Ren, and Jian Sun. "Deep residual learning for image recognition." In Proceedings of the IEEE conference on computer vision and pattern recognition, pp. 770-778. 2016.

[5] Szegedy, Christian, Vincent Vanhoucke, Sergey Ioffe, Jon Shlens, and Zbigniew Wojna. "Rethinking the inception architecture for computer vision." In Proceedings of the IEEE conference on computer vision and pattern recognition, pp. 2818-2826. 2016.

[6] Tan, Mingxing, and Quoc V. Le. "Efficientnet: Rethinking model scaling for convolutional neural networks." arXiv preprint arXiv:1905.11946 (2019).

