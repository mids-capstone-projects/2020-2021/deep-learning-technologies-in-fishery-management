# Deep learning technologies in fishery management

*Repository for the Duke MIDS Capstone 2020-2021 project: "Deep learning technologies in fishery management"*

##### Team Member: Zhiwei Calvin Dong, Jingjing Shi, Abhiraj  Vinnakota

##### Team Manager: Dr. Yuqiang Zhang

##### Client: Dr. Steven Box (Fish Forever at Rare, Inc.), Dr. Steve Canty (Smithsonian Institute)

##### Check out presentation recordings for the project here

<div align="left">
      <a href="https://www.youtube.com/watch?v=ZOxRP16XbdY">
         <img src="https://img.youtube.com/vi/ZOxRP16XbdY/0.jpg" style="width:100%;">
      </a>
</div>

## Background 

Fisheries management includes monitoring exploited fish populations, estimating maximum sustainable yield, and setting catch limits. To effienctly manage fisheries, tracking key metrics such as the number of people fishing, the quantity of fish that is being caught, the different species that are being caught as well as the locations of fishing activity are of vital importance. 

In this project, a variety of computer vision algorithms will be applied to identify fishing grounds based on fish morphometric using photographic images. The main goal of this project is to automate the classification of images of fish to the correct species, and subsequently assign them to their country and fishing ground of origin.

The hierarchical model consists of one object detection model and four classification models that can successfully identify fish and predict fish species, countries as well as fishing grounds. All five models have high accuracy. The object detection model achieves 84.0% accuracy. The four classification models achieve 86.7% for classification of fish species, 97.6% for classification of countries of yellowtail snapper, 98.8% for classification of countries of white grunt, and 97.4% for classification of fishing grounds of yellowtail snapper in Honduras. Once more fish images are brought in, the model will be more accurate and generalized, which benefits fishery management in the future. In addition, we deploy the model to an HTML application that could be easily connected to a remote server. We also find deep neural networks with deeper and wider layers, such as Inception-V3, have better performance than those with shallower layers and chain-like structures when dealing with complex dataset. 

## Methods

Object detection is applied to first identify if the image has a fish or not and more importantly locate the fish in the given image, by drawing a bounding box around it.

We developed four classfication models: species classification model, country classification model for yellowtail snapper, country classification model for white grunt and fishing ground classification model for yellowtail snapper in Honduras. We also explored patterns of misclassified images in order to improve the model performances.

The hierarchical model structure is shown below.

<img src="./FinalPaper/Hierarchical_model.png" width="720" >

## Repository Structure

1. The 'FinalPaper' folder saves the final paper and corresponding materials.

2. The 'StoryBoard' folder saves the storyborad.

3. The 'Slides_recording' folder saves the project presentation slides and recording.

4. The 'Example_data' folder saves example data.

5. The 'Models' folder has all the models we have tried for this project, and there are two sub_folders, 'Classification' folder contains models/codes/logs for ResNet34 and InceptionV3 and 'ObjectDetection' folder contains models/codes/results for YOLO.

6. The 'Deployment' folder saves codes for model deployment.

## Guidance of running programs

1. If you want to run the final application, go to the Deployment folder and run "python Deployment_v2.py" and then see the app from localhost:5000/predict 

2. If you want to train classification model, go to Models/Classification/run_gpu/codes. You may find training codes for species (named as ...sp....py), country (named as ...ct....py) and fishing grounds (named as ...fg....py). You may also find codes for data splitting, EDA, and misclassification check from Models/Classification. They are python Jupyter Notebooks. Just open them and run!

3. If you want to use our classification models, run pred_check_misclassification.ipynb under Models/Classification folder

4. If you want to train Object Detection model, go to Models/ObjectDetection and run train.py.

5. If you want to use our Obejct Detection models, go to Models/ObjectDetection and run 

!python detect.py --cfg cfg/yolov3-custom_run_fish.cfg --conf-thres 0.1 --names data/customdata/run_fish/custom.names --output data/customdata/run_fish/detect/test/ --save-txt --source ../../../test/ --weights weights/best_run_fish.pt

from your terminal.

Please note: If your goal is one of 2 - 5 shown above, you need to prepare your own training/testing sets. Pleaes save your images in one folder and put under the same folder with codes.

For preparing datasets of classification models, if you are not familiar with model training, you can follow setps below:

There should be three folders named as:
1. 'train' which is randomly selected from all raw data (we randomly selected 80%) for training process
2. 'val' which is randomly selected from all raw data (we randomly selected 20%) for validation
3. 'test' which includes all raw data

Every folder above should has saved images in correct subfolders which are labeled as 'Belize' and 'Honduras' for example of training country classification models

For 'test',  we only use them to do prediction and disregard what are original labels, it's ok to save images in any labels, but still need both under upper level folders.

***If any questions for this repo, please contact calvindongt@gmail.com***
