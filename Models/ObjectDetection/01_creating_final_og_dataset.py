# %% Importing requirements

import pandas as pd
import numpy as np
import glob
from PIL import Image
import random
import matplotlib.pyplot as plt
import shutil, os

# %% ######## 
# #####One time, moving other_yellowtail_images to images and labelling correctly.

#Getting images as glob 
other_yt = glob.glob('./data/customdata/detect/other_yellowtail/*.jpg')
len(other_yt)

#%% Changing 

#'./data/customdata/detect/other_yellowtail/Assisted_Cozen Patch_IMG_20181108_BZ248.jpg'
## to 
#'./data/customdata/images/Assisted_Cozen Patch_IMG_20181108_BZ248.jpg'

modified_other_yt = []
for i in other_yt:
    modified_other_yt.append(i.replace('detect/other_yellowtail', 'images'))

# %% Writing to seperate file 

with open("./data/customdata/00_original_labels/other_yt_pics.txt", 'x') as f:
    for i in modified_other_yt:
        f.write(f'{i}\n')
    f.close()

# %% Combing with original yellowtail.txt file

filenames = ["./data/customdata/00_original_labels/yellowtail_snapper_pics.txt", "./data/customdata/00_original_labels/other_yt_pics.txt"]
with open("./data/customdata/00_original_labels/all_yt_snapper_pics.txt", 'w') as outfile:
    for fname in filenames:
        with open(fname) as infile:
            outfile.write(infile.read())
    outfile.close()

# %%
# Moving labels for other_yellowtail into original labels 

other_yt_labels = glob.glob('./data/customdata/detect/run_1/out_other_yellowtail/*.txt')
len(other_yt_labels)

# %% Changing

# './data/customdata/detect/run_1/out_other_yellowtail/Assisted_Flat Patch_IMG_20181108_BZ256.jpg.txt'
## to
# './data/customdata/00_original_labels/labels_yellowtail/Assisted_Flat Patch_IMG_20181108_BZ256.txt'

modified_other_yt_labels = []
for i in other_yt_labels:
    i = i.replace('.jpg.txt', '.txt')
    i = i.replace('detect/run_1/out_other_yellowtail', '00_original_labels/labels_yellowtail')
    modified_other_yt_labels.append(i)

# %% 
other_yt_images = []
for i in other_yt_labels:
    i = i.replace('.jpg.txt', '.jpg')
#    i = i.replace('detect/run_1/out_other_yellowtail', '00_original_labels/labels_yellowtail')
    other_yt_images.append(i)

# %% Creating label files for other_yellowtail
counter =0 

def convert(x_min,y_min,x_max,y_max,width,height,class1):
    one = str(class1)
    two = str((x_min + x_max) / 2 / width)
    three = str((y_min + y_max) / 2 / height)
    four = str((x_max - x_min) / width)
    five = str((y_max  - y_min) / height)
    string = one+ ' '+ two+ ' '+ three+ ' '+ four+ ' '+ five
    return string

# %%
for i in range(len(other_yt_labels)):
    original = other_yt_labels[i]
    new = modified_other_yt_labels[i]
    img_path = other_yt_images[i]

    with open(original, 'r') as f:
        # Get 1st line of prediction
        list_lines = f.readlines()
        line = list_lines[0]
        split = line.split()

        # Get coordinates and class
        x_min = int(split[0])
        y_min = int(split[1])
        x_max = int(split[2])
        y_max = int(split[3])
        class1 = int(split[4])

        # Get img dims
        img = Image.open(img_path)
        width , height = img.size
        img.close()

        # Convert and save
        new_string = convert(x_min,y_min,x_max,y_max,width,height,class1)

        counter = counter+1
        print(counter)

    with open(new,'w') as f:
        f.write(new_string)

# %% One time, moving other_whitegrunt labels into labels of whitegrunt

wg_labels = glob.glob('./data/customdata/detect/run_1/out_whitegrunt/*.txt')
len(wg_labels)

modified_wg_labels = []
for i in wg_labels:
    i = i.replace('.jpg.txt', '.txt')
    i = i.replace('detect/run_1/out_whitegrunt', '00_original_labels/labels_whitegrunt_vals')
    modified_wg_labels.append(i)

wg_images = []
for i in wg_labels:
    i = i.replace('.jpg.txt', '.jpg')
    # i = i.replace('detect/run_1/out_other_yellowtail', '00_original_labels/labels_yellowtail')
    wg_images.append(i)

# %% Creating vals labels for white_grunt

# Creating labels
for i in range(len(wg_labels)):
    original = wg_labels[i]
    new = modified_wg_labels[i]
    img_path = wg_images[i]

    with open(original, 'r') as f:
        # Get 1st line of prediction
        list_lines = f.readlines()
        line = list_lines[0]
        split = line.split()

        # Get coordinates and class
        x_min = int(split[0])
        y_min = int(split[1])
        x_max = int(split[2])
        y_max = int(split[3])
        class1 = int(split[4])

        # Get img dims
        img = Image.open(img_path)
        width , height = img.size
        img.close()

        # Convert and save
        new_string = convert(x_min,y_min,x_max,y_max,width,height,class1)

        counter = counter+1
        print(counter)

    with open(new,'w') as f:
        f.write(new_string)

# %% Make number of images in vals and zeros of whitegrunt_same 

#Getting images as glob 
wg_vals = glob.glob('./data/customdata/00_original_labels/labels_whitegrunt_vals/*.txt')
wg_zeros = glob.glob('./data/customdata/00_original_labels/labels_whitegrunt_zeros/*.txt')

len(wg_vals)
len(wg_zeros)

modified_wg_zeros= []
for i in wg_zeros:
    modified_wg_zeros.append(i.replace('_zeros', '_vals'))

missing = [i for i in wg_vals if i not in modified_wg_zeros]

# %% remove files

for i in missing:
    os.remove(i)