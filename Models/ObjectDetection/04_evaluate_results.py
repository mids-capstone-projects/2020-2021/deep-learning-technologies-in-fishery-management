# %% Importing libraries
import pandas as pd
import numpy as np
import glob
from PIL import Image
import random
import matplotlib.pyplot as plt
import shutil, os
import sys

# %% ### Results 
#
def get_labels(label_type, file_list, i, j):
    
    labels =[]
    new_files = []
    
    if label_type == 'actual':
        # Replace links
        for name in file_list:
            new_files.append(name.replace('images',f"run_{i}/labels").replace('.jpg','.txt')[:-1])
        
        # Read labels
        for file in new_files:
            if os.stat(file).st_size == 0:
                labels.append('')
            else:
                with open(file,'r') as f:
                    line = f.read()
                    labels.append(line[0])
                    print(line[0])

    if label_type == 'pred':
        # Replace links
        for name in file_list:
            new_files.append(name.replace('images',f"run_{i}/detect/{j}").replace('.jpg','.jpg.txt')[:-1])
            
        for file in new_files:
            if os.stat(file).st_size == 0:
                labels.append('')
            else:
                with open(file,'r') as f:
                    line = f.read()
                    list1 = line.split()
                    labels.append(list1[4])
                    print(list1[4])
    return labels


# 
actual_labels = {'fish' : { 'train' : [] , 'test' : [], 'valid' : []},
                 'species' : { 'train' : [] , 'test' : [], 'valid' : []}}

pred_labels = {'fish' : { 'train' : [] , 'test' : [], 'valid' : []},
                 'species' : { 'train' : [] , 'test' : [], 'valid' : []}}


for label_type in ['pred', 'actual']: #actual_labels, pred_labels]:
    for i in ['fish', 'species']:
        for j in ['train','test','valid']:

            print(label_type, i, j)

            with open(f"./data/customdata/run_{i}/{j}.txt", 'r') as f:
                file_list = f.readlines()

                if label_type == 'actual':
                    actual_labels[i][j] = get_labels(label_type, file_list, i, j)
                elif label_type == 'pred':
                    pred_labels[i][j] = get_labels(label_type, file_list, i, j)



# %%
for i in ['fish', 'species']:
    for j in ['train','test','valid']:
        act = pd.Series(actual_labels[i][j])
        pre = pd.Series(pred_labels[i][j])
        
        print(f"{i} model: {j} - actual: {act.value_counts()}")
        print(f"{i} model: {j} - prediction: {pre.value_counts()}")



# %%
for i in ['fish', 'species']:
    for j in ['train','test','valid']:
        act = pd.Series(actual_labels[i][j])
        pre = pd.Series(pred_labels[i][j])
        
        print(f"{i} model: {j} - actual: {act.value_counts()}")
        print(f"{i} model: {j} - prediction: {pre.value_counts()}")

# %% Cropping data based on prediction of fish and no-fish



# %% Run a classification model and see if classification improves
 
# Explore other techniques in the data aug course 
# Explore how to get rid of 0 

# Setup for run_fish
# Setup for run_species


# %%
# Move ground-truth species - train into mAP folder for conversion 

run_name = "run_fish"
set_name = "train"

# %%
## Moving GT
with open(f"./data/customdata/{run_name}/{set_name}.txt", 'r') as f:
    file_list = f.readlines()

count = 0

os.mkdir(f"./mAP/input/ground-truth/")

for file in file_list:
    src = file.replace('images',f"{run_name}/labels").replace('.jpg','.txt')[:-1]
    dest = src.replace(f"./data/customdata/{run_name}/labels/", f"./mAP/input/ground-truth/")

    shutil.copy(src,dest)

    count= count+1
print(count)

# %%
## Moving Detections
run_name = "run_fish"
for set_name in ["train","test","valid"]:

    detect_labels = glob.glob(f"./data/customdata/{run_name}/detect/{set_name}/*.txt")
    count = 0
    empty = 0
    print(len(detect_labels))

    #os.mkdir(f"./mAP/input/detection-results{run_name}{set_name}/")

    for file in detect_labels:

        size = os.path.getsize(file)
        if size ==0:
            empty = empty+1
            print(path)
        
        final_lines = []
        with open(file, 'r') as f:
            # Get 1st line of prediction
            list_lines = f.readlines()
            for i in list_lines: 
                split = i.split()

                # Get coordinates and class
                x_min = split[0]
                y_min = split[1]
                x_max = split[2]
                y_max = split[3]
                class1 = split[4]
                conf = split[5]

                if class1 == '0':
                    class1 = 'lane_snapper'
                elif class1 == '1':
                    class1 = 'yellowtail_snapper'
                else:
                    print(class1)
                    print("problem_with this class")
                    sys.exit()


                # <class_name> <confidence> <left> <top> <right> <bottom>
                str1 = class1 +' '+ conf +' '+ x_min +' '+ y_min +' '+ x_max +' '+ y_max
                final_lines.append(str1)

        dest = file.replace(".jpg.txt", ".txt").replace(f'./data/customdata/{run_name}/detect/{set_name}/',f"./mAP/input/detection-results{run_name}{set_name}/")

        with open(dest, 'w') as f:
            for i in final_lines:
                f.write(f"{i}\n")
        count= count+1
    print(count)

## 1339
## 1239

# %% Creating missing files in detetcion results for fair comparison 

for i in ['','run_speciestest','run_speciesvalid']:

    gt_labels = glob.glob(f"./mAP/input/ground-truth{i}/*.txt")
    dt_labels = glob.glob(f"./mAP/input/detection-results{i}/*.txt")
    print(len(gt_labels))
    print(len(dt_labels))

    gt_labels_modified = []
    for file in gt_labels:
        file = file.replace(f"ground-truth{i}",f"detection-results{i}")
        gt_labels_modified.append(file)

    missing = [ k for k in gt_labels_modified if k not in dt_labels]
    print(i)
    print(len(missing))

    for file in missing:
        with open (f"{file}","x") as f:
            f.close()

# print(len(txt_files))
# print(len(photos))
# %% Wrong format files 

run_name = "run_fish"
for set_name in ["test","valid"]:

    print(set_name)
    dt_labels = glob.glob(f"./mAP/input/detection-results{run_name}{set_name}/*.txt")

    for i in dt_labels:
        with open(i,"r") as f:
            list1 = f.read().split()
            if len(list1) > 6 :
                print(i)



# %%
