# %% Importing libraries
import pandas as pd
import numpy as np
import glob
from PIL import Image
import random
import matplotlib.pyplot as plt
import shutil, os

# %% Replacing different words with same words

# Replace

original = glob.glob('./data/customdata/00_original_labels/labels_yellowtail/*.txt')
print(len(original))

modified = []
for i in original:
    i = i.replace('Asistido_', 'Assisted_')
    i = i.replace('Inasistido_', 'Unassisted_')
    i = i.replace('Assisted-', 'Assisted_')
    i = i.replace('Unassisted-', 'Unassisted_')
    modified.append(i)

# %%
# Rename
count = 0
for ori, mod in zip(original,modified):
    if ori != mod:
        count = count+1
        os.rename(ori,mod)
print(count)

# %% Replaceing texts

for name in [ "lane_snapper_pics.txt","white_grunt_pics.txt","yellowtail_snapper_pics.txt"]:

    with open(f"./data/customdata/00_original_labels/{name}", 'r') as f:
        lines = f.readlines()

    modified_lines = []
    for i in lines:
        i = i.replace('Asistido_', 'Assisted_')
        i = i.replace('Inasistido_', 'Unassisted_')
        i = i.replace('Assisted-', 'Assisted_')
        i = i.replace('Unassisted-', 'Unassisted_')
        modified_lines.append(i)
    
    with open(f"./data/customdata/00_original_labels/{name}", 'w') as f:
        for i in modified_lines:
            f.write(i)

# %% Creating datasets- Train (60%), Test (20%), Valid (20%)

# Read pic data 

lane_pics = []
whitegrunt_pics = []
yellowtail_pics = []

pic_dict = {"lane_snapper_pics.txt": lane_pics,
            "white_grunt_pics.txt" : whitegrunt_pics,
            "yellowtail_snapper_pics.txt" : yellowtail_pics }

for file , data in pic_dict.items():
    with open(f'./data/customdata/00_original_labels/{file}','r') as f:
        data.extend(f.readlines())
        print(len(data))

# %%
#Remove assisted, unassisted and get half the number of images

pic_dict_reduced = {}

for file , data in pic_dict.items():
    modified = []
    for i in data :
        i = i.replace('\n','')
        i = i.replace('Assisted_', '')
        i = i.replace('Unassisted_', '')
        modified.append(i)

    res = []
    [res.append(x) for x in modified if x not in res]

    pic_dict_reduced[file] = res
    print(len(res))


# %% Values for 60% , 20% , 20%

for i, j in pic_dict_reduced.items():
    print(f"For {i}\n")
    print(f"Train {len(j)*0.6}\n")
    print(f"Test {len(j)*0.2}\n")
    print(f"Valid {len(j)*0.2}\n")

# %% Sampling, trainin (60%) , testing (20%) and validation (20%) - stratified

def Diff(li1, li2):
    return (list(list(set(li1)-set(li2)) + list(set(li2)-set(li1))))

train = []
test = []
valid = []

lane_pics = pic_dict_reduced["lane_snapper_pics.txt"]
whitegrunt_pics = pic_dict_reduced["white_grunt_pics.txt"]
yellowtail_pics = pic_dict_reduced["yellowtail_snapper_pics.txt"]

datasets = { (234,51,389) : train,
             (78,17,129) : test }
#             (78,17,129) : valid }

for sample, dataset in datasets.items():
        
    sample_lane = random.sample(lane_pics ,sample[0])
    sample_whitegrunt = random.sample(whitegrunt_pics,sample[1])
    sample_yellowtail = random.sample(yellowtail_pics,sample[2])
    print(len(sample_lane))
    print(len(sample_whitegrunt))
    print(len(sample_yellowtail))

    for i in [sample_lane,sample_whitegrunt,sample_yellowtail]:
        dataset.extend(i)
        pass

    lane_pics = Diff(lane_pics, sample_lane)
    whitegrunt_pics = Diff(whitegrunt_pics, sample_whitegrunt)
    yellowtail_pics = Diff(yellowtail_pics, sample_yellowtail)
    print(len(lane_pics))
    print(len(whitegrunt_pics))
    print(len(yellowtail_pics))
    pass

print(len(lane_pics))
print(len(whitegrunt_pics))
print(len(yellowtail_pics))

for i in [lane_pics,whitegrunt_pics,yellowtail_pics]:
    valid.extend(i)

# %%
print(len(train))
print(len(test))
print(len(valid))

# %% Bloating it up by adding assisted and unassisted

final_train = []
final_test = []
final_valid = []

final_dict = {'train.txt': [final_train, train],
'test.txt': [final_test, test],
'valid.txt': [final_valid, valid]}

for _, data_list in final_dict.items():
    final = data_list[0]
    initial = data_list[1]
    for i in ['images/Assisted_','images/Unassisted_']:
        for file in initial:
            file = file.replace('images/',i)
            final.append(file)
    print(len(final))

# %% Printing

for i in [final_train,final_test,final_valid]:
    print(len(i))

# %% Check if path actually exists else remove
count = 0

for i in [final_train,final_test,final_valid]:
    for path in i:
        if not os.path.isfile(path):
            count = count+1
            i.remove(path)
print(count)

# %% Saving them as final train, test and validation image sets, for all

for name,data in zip(['train.txt','test.txt','valid.txt'],[final_train,final_test,final_valid]):
    with open(f"./data/customdata/{name}","w") as f:
        for i in data:
            f.write(f"{i}\n")

# Final
# 1339 - Train
# 443 - Test
# 447 - Valid