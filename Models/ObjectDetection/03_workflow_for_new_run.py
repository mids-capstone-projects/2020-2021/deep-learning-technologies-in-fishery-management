
# %% Importing libraries
import pandas as pd
import numpy as np
import glob
from PIL import Image
import random
import matplotlib.pyplot as plt
import shutil, os
import cv2

# %% Resizing original images 

def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation = inter)

    # return the resized image
    return resized

#os.mkdir("./data/customdata/images/")


for i in ['lane_snapper','white_grunt','yellowtail_snapper']:
    count = 0
    with open(f"./data/customdata/00_original_labels/{i}_pics.txt","r") as f:
        files = f.readlines()

        for file in files:
            file = file.replace(".jpg\n",".jpg")
            dest = file
            print(dest)

            src = file.replace("./data/customdata","./mAP/input")
            print(src)

            image = cv2.imread(src)
            shape = image.shape
            print(shape)
            ht = shape[0]
            wd = shape[1]

            print(ht)
            print(wd)

            if ht < wd:
                height = 600
                width = None
            else:
                width = 600
                height = None

            image = image_resize(image, width, height, inter = cv2.INTER_AREA)
            print(image.shape)
            cv2.imwrite(dest,image)
            count = count + 1
    print(count)


# %%   run_species_all
# Step 1: Creating labels

# Loading Labels
lane = "labels_lane"
whitegrunt = "labels_whitegrunt_vals"
yellowtail = "labels_yellowtail"

labels_lane = glob.glob(f"./data/customdata/00_original_labels/{lane}/*.txt")
labels_whitegrunt = glob.glob(f"./data/customdata/00_original_labels/{whitegrunt}/*.txt")
labels_yellowtail = glob.glob(f"./data/customdata/00_original_labels/{yellowtail}/*.txt")

print(len(labels_lane))
print(len(labels_whitegrunt))
print(len(labels_yellowtail))
# %% Giving proper label and saving them 

final = 'run_species_all/labels'

# %%
for i, j, cat in zip([labels_lane,labels_yellowtail, labels_whitegrunt],[lane, yellowtail, whitegrunt],[0,1,2]):
    for file in i:
        dest = file.replace(f'00_original_labels/{j}', final)
        with open(file,'r') as f:
            old_line = f.read()
            out_file = open(dest,'w')
            out_file.write(f'{cat} '+ old_line[2:])

# %%Empty labels  

# for file in labels_whitegrunt:
#     dest = file.replace(f'00_original_labels/{whitegrunt}', final)
#     out_file= open(dest,'w')
#     out_file.close()


# %% Checking if all labels went in

# labels = glob.glob(f"./data/customdata/images/*.jpg")
# print(len(labels))

run = 'run_species_all'

# with open(f"./data/customdata/{run}/train.txt", 'w') as f:
#     for i in labels:
#         f.write(f"{i}\n")
# Also check randomly if everything when fine

# %% Step 2: Duplicating train.txt, test.txt and valid.txt


shutil.copyfile("./data/customdata/train.txt", f"./data/customdata/{run}/train.txt")
shutil.copyfile("./data/customdata/test.txt", f"./data/customdata/{run}/test.txt")
shutil.copyfile("./data/customdata/valid.txt", f"./data/customdata/{run}/valid.txt")

# %% Step 3: Writing the custom data file 

classes = 3

with open(f"./data/customdata/{run}/custom.data", 'x') as f:
    f.write(f'classes={classes}\n')
    f.write(f'train=data/customdata/{run}/train.txt\n')
    f.write(f'valid=data/customdata/{run}/test.txt\n')
    f.write(f'names=data/customdata/{run}/custom.names\n')
    f.close()

# %% Step 4: Writing the custom names file

names = ["lane_snapper","yellowtail_snapper","white_grunt"] 

with open(f"./data/customdata/{run}/custom.names", 'w') as f:
    for i in names:
        f.write(f"{i}\n")

# %% Step 5: Having a different config file for the run 

# Step1: Duplicate config file 
# Step2: 3 entries :Filters = (4+1+ #classes)* 3     = 18 for 1 class
# Step3: 3 entries :classes = #classes
# Step4:
    # burn_in to 100
    # max_batches to 5000
    # steps to 4000,4500

# %%


# %% Step 6: Customize the train.py file 

# Make sure the 'weights' are saved in a seperate file name.

# %% Step 7: Duplicate the colab file and input proper variables to make sure everything works well. 

## Before training, make sure the labels folder is present in the custom folder and is names as "labels"

## Below doesnt work though!!
count =0
# Step 7: Very very important
# Check if all the pabels have an associated images or not!!
for i in ['run_species_all']:

    for j in ['train','test','valid']:
        with open(f"./data/customdata/{i}/{j}.txt") as f:
            files = f.readlines()
            for file in files:
                if not os.path.isfile(file[:-1]):
                    print(file)
                    break
                else:
                    count = count+1
        print(count)
        count =0


