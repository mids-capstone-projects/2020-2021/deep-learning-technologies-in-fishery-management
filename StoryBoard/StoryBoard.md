## Epic 1

As a data scientist working with Smithsonian Institute, we want to help the clients to assign fishes in their fishing ground in Belize and Honduras, so that we can determine the fishing pressure in different locations and help set fishing regulations in these countries.

### Plan:
To successfully assign fished to their fishing ground of origin with a high level of accuracy from photographic images. Firstly, we will distinguish fishes from the fisheries of Belize and  Honduras, and secondly, assign fish to their fishing grounds.

#### Theme 1.1

We need to understand the data and figure possible issues with images, such as inconsistent backgrounds, orientations of images, and the brightness of images. We also need to try a 'toy' model to understand how deficiencies in our data will affect our analysis.

##### Story 1.1.1
Find the distributions of classes in our dataset, both at country level and fishing ground level. Summarize the information into a dataframe, and save to a csv. If extreme imbalances are founded, we need to consult the clients to check if we can get more data.

*Status:* Finished by all members in this project.

The results are in [Data Exploration.pptx] (https://gitlab.oit.duke.edu/duke-mids/workingprojectrepositories/2020-2021/fish-morphometrics/-/blob/master/DataExploration/DataExploration.pptx).


*Is is blocked by another story?*

No

*Other comments:*

We found other issues like wrong-labelling when we started modeling, so the actual distributions (fishing ground assignment) were modified based on GPS data. The final distribution can be found in [1026_meeting.pptx] (https://gitlab.oit.duke.edu/duke-mids/workingprojectrepositories/2020-2021/fish-morphometrics/-/blob/master/Meetings/1026_meeting.pptx)


##### Story 1.1.2

Try out initial simple model to understand deficiencies, and look through the misclassified images to see what other transformations can be done.

*Status:* 

Finished by Calvin Dong (ResNet and InceptionV3 in Pytorch), git commit: 1e4f04ab, f33d87c9, f4e55779, 5f16115a
		Jingjing Shi (InceptionV3 in Keras). 160dc9c6

ResNet34 and InceptionV3 models were tried and accuracies are around 90\%.


*Is is blocked by another story?*

No

*Other comments:*
The two models were tuned further to get higher accuracy, final results were in the ['final_presentation.pptx'] (https://gitlab.oit.duke.edu/duke-mids/workingprojectrepositories/2020-2021/fish-morphometrics/-/blob/master/Meetings/final_presentation.pptx)

#### Theme 1.2
We will start to distinguish the fishes in the two countries. We need to improve our initial model by utilizing other data processing strategies, and we need to achieve high accuracy that satisfy the clients.

##### Story 1.2.1
To ensure the model is resilient to the background. The data have two kind of background, one is white background and the other one is blue/white strips. We need to make sure that our model accuracy is not affected by the background of images.

*Status (Specify start date here):*

Finished by all members in this project. Final summary can be found in the ['final_presentation.pptx'] (https://gitlab.oit.duke.edu/duke-mids/workingprojectrepositories/2020-2021/fish-morphometrics/-/blob/master/Meetings/final_presentation.pptx).

*Is is blocked by another story?*

No.


*Other comments:*

##### Story 1.2.2
To apply image augmentation techniques to improve accuracies.

*Status (Specify start date here):*

Finished by all members in this project. Final summary can be found in the ['final_presentation.pptx'] (https://gitlab.oit.duke.edu/duke-mids/workingprojectrepositories/2020-2021/fish-morphometrics/-/blob/master/Meetings/final_presentation.pptx).

*Is is blocked by another story?*

No.

*Other comments:*

Data transformations used in these models are: horizontal flip, random rotation, random crop, and gray-scale.

##### Story 1.2.3

To figure out a way to handle class imbalances.

*Status (Specify start date here):*

Finished by Calvin Dong and Jingjing Shi. Final summary can be found in the ['final_presentation.pptx'] (https://gitlab.oit.duke.edu/duke-mids/workingprojectrepositories/2020-2021/fish-morphometrics/-/blob/master/Meetings/final_presentation.pptx).

*Is is blocked by another story?*

No

*Other comments:*

We incorporate GPS information to collapse fishing grounds.

##### Story 1.2.4
To apply regularization techniques such as cutout, dropout, and random rotation to avoid overfitting.

*Status (Specify start date here):*

No overfitting was observed during model training as of Nov. 19th 2020.

*Is is blocked by another story?*

*Other comments:*

#### Theme 1.3 
We will then move on the assign fishes into their fishing grounds. We will first apply the models we have from Theme 1.2, and improve the accuracies until the clients are satisfied.

##### Story 1.3.0
Collapse fishing grounds based on GPS information.

*Status (Specify start date here):*

Finished by Jingjing Shi with the help from all members in the project. git commit a5f04e61.

*Is is blocked by another story?*

No.

*Other comments:*

This is a new request from the client. Some fishing grounds have fewer than 10 images, so collapse those fishing ground based on geographic location will give us fewer fishing grounds with more images per ground.


##### Story 1.3.1

Apply the model from Theme 1.2 to assign fishes into their fishing grounds, and check accuracy.

*Status (Specify start date here):*

Finished by Calvin Dong (ran models) git commit :  dcd0dbcb
		and Jingjing Shi (balance data distributions). git commit: 5b346ecf, 8767f9d7
Final results can be found in the ['final_presentation.pptx'] (https://gitlab.oit.duke.edu/duke-mids/workingprojectrepositories/2020-2021/fish-morphometrics/-/blob/master/Meetings/final_presentation.pptx).

*Is is blocked by another story?*

No.

*Other comments:*

#### Theme 1.4 
To identify if the given image is of a fish or not. If there is the fish, we need to localize it using bounding boxes. To achieve this, we would need to construct an object-detection model 

##### Story 1.4.1

Initial Research on object detection model

Status: Completed by Abhiraj Vinnakota

Looked at Faster-RCNN as well as YOLO. Deep-dived and gathered requirements to build the model.

Presentation is available here: https://gitlab.oit.duke.edu/duke-mids/workingprojectrepositories/2020-2021/fish-morphometrics/-/blob/master/Meetings/Object%20Detection.pptx 

Presented the inner workings of the YOLOv5 and sensitized the clients towards the effort that is needed as well as the end result that can be expected

##### Story 1.4.2

Manually label fish images to generate labels for bounding boxes. 

Status: Completed by Abhiraj Vinnakota

Manually labelled data is available here: https://gitlab.oit.duke.edu/duke-mids/workingprojectrepositories/2020-2021/fish-morphometrics/-/tree/master/Fish_Images/labels_object_detection

Used an online tool “makesense.ai” to draw bounding boxes on more than ~900 images for both 
 
##### Story 1.4.3

Build the object detection model

Status: Completed by Abhiraj Vinnakota

Code for the model is available here: https://gitlab.oit.duke.edu/duke-mids/workingprojectrepositories/2020-2021/fish-morphometrics/-/tree/master/Models/ObjectDetection/object_detection_labels

The final model achieves a mAP of 0.84. It successfully classified all the test images based on the fish species. 

##### Story 1.4.4 

Run the model on different datasets to evaluate performance
- On the yellowtail images, that weren't labelled
- On images of the internet
- On the white grunt images

Status: Completed by Abhiraj Vinnakota on 30th Jan-2021

Comments: The results haven't been great, ie, the model seems to be good with detecting the fish location (objectness), but the model isn't doing great with detecting species. extact giving rise to stories 4.5, 4.6, 4.7, 4.8

##### Story 1.4.5 <<<moved to 1.5.1 as it turned out to fit better there >>>

##### Story 1.4.6 

Manually tag images of yellowtail species from Belize. There are around 800 images. 

Status : Completed <Abhiraj Vinnakota on Feb7th-2021>

Comment: On the project managers suggestion, I used a model to get the bonding boxes, as such, all the images are now tagged.

##### Story 1.4.7

Incorporate customized bounding box sizes instead of using the standard ones to see if it improves performance.

Status : On hold <Abhiraj Vinnakota as on Feb7th-2021>

Comment : Upon researching further, it turned out that the objectness of the model is what is improved with customizing default bonding boxes with the model, not its classfication. Therefore, we are putting this on the back burner as we are already satisfied with the objectness.

##### Story 1.4.8

Train a model that just detects if there is a fish or not, irrespective of the species.
Also, keeping in mind to make sure the validation set is completely exclusive, i.e., both unassisted and assisted images should be outside the train and test sets.

Status : Completed as of Feb14th-2021 <Abhiraj Vinnakota>
Comments: The seperation of assisted and unassisted images in the different sets is completed. The model has been run. After looking at sample images from the validation test, we seem confident that it can give good results. Yet to calculate the mAP for the test and validation dataset.

##### Story 1.4.9

Compile validation set results in terms of mAP (mean average precision) and plot data.

Status : Completed as of Feb21th-2021 <Abhiraj Vinnakota>
Comments: The darknet framework does not provide a decent way to calculate the mAP of validation or test sets. As such, a publicly available github repo https://github.com/Cartucho/mAP is being used to do this. 

##### Story 1.4.10
Evaluate the performance of the model on random images from the internet
Status : Completed as of Feb21st-2021
Comments: The conlcusion is that the model is a bit frivolous when it comes to detecting a fish, as it even detected a crab as well as a human face as a 'fish'.

##### Story 1.4.11

Train the fish detection model with publicly available no-object datasets to see if its objectness can be improved. 

Status: In progress as of Feb21th-2021 <Abhiraj Vinnakota>
Comments: When we tested images on the internet, we found the model to be a bit frivolous when it comes to detecting a fish. As such, we want to explore and see if the objectness can be improved by training the model on random objects that are not fishes. 

##### Story 1.4.12

Interpret the model using saliency maps to understand what the model is looking for, it may throw clues on how to model maybe improved. 

Status: In progress as of Feb21th-2021 <Abhiraj Vinnakota>
Comments: We founda an open soruce implementation for YOLOv3 models that we hope to use. https://github.com/jennalau/feature-vis-yolov3
complementary article : https://medium.com/@jl22/feature-visualization-with-yolov3-fa133f15cae4


#### Theme 1.5 

Build a classification model that is capable of detecting the fish species. 
 
##### Story 1.5.1  <<< Deprecated>>> (Validation results needed this to be reopened)

Build a classification model that is capable of detecting the fish species (object detection) 

Status: Completed indirectly by building the Object Detection model as it was built to identify the species of the fish as well.

Code of the model is available here: https://gitlab.oit.duke.edu/duke-mids/workingprojectrepositories/2020-2021/fish-morphometrics/-/tree/master/Models/ObjectDetection/object_detection_labels

##### Story 1.5.1.0  <<< New >>

The client has pointed out inefficiences in having a seperate detection model for each species. However, a single object detection model doesn't seem to faring well in detecting species. Therefore, we want to see if there is anything that can be done to improve the efficieny of the models. The techniques identified are
	- "Evolve" feature in YOLO to identify best possible hyper-parameters for in-built data augmentations.
	- "Albumentations" to incorporate additional data augmentations and increase sample size (theoretically)

Status : Started by Abhiraj Vinnakota on 31th Jan-2021
Comment: Have figured out how to do 'Albumentations'. Tried doing it as well. However, when I increased the dataset to 5x, the model was taking forever to train as the size of the images being used by me were the original size. As such, I reduced the size the size to 600px (smaller side). 
I am yet to train the species model using albumentations on the newly sized images. 

##### Story 1.5.1.1

Compile validation set results in terms of mAP (mean average precision) for the species-detection model.
Status : Completed as of Feb21st-2021 
Comments: The darknet framework does not provide a decent way to calculate the mAP of validation or test sets. As such, a publicly available github repo https://github.com/Cartucho/mAP is being used to do this.

##### Story 1.5.1.2
Evaluate the performance of the model on random images from the internet
Status : Completed as of Feb21st-2021
Comments: The conlcusion is that the model is quite picky in terms of what it thinks is a fish, but it isn't doing well in terms of classification accuracy. Need to see if 'albumentations' or 'evolve' can help do the trick.

##### Story 1.5.2

Create train and validation datasets for the classification of fish species (two version: 1. Belize and Honduras; 2. Only Belize) Perform data cleaning (color panel on top).
Status: Completed by Calvin and Jingjing on 2/3/2021 to create train and val dataset.

	Complete by Jingjing on 2/13/2021 to clean the data.


##### Story 1.5.3

Build a classification model that is capable of classify the fish species (classification model) 

Status: Started by Calvin on 2/4/2021. We tried to 1. classify White Grunt and Yellow Snapper in both Belize and Honduras; 2. classify White Grunt, Yellow Snapper and Lane Snapper in Belize. Two models were created however both of them only achieved about 70% accuracy. The first model was failed to classify White Grunt (only about 50% accuracy). The second model had issues with classifying Yellow Snapper and White Grunt (see results from ./Models/Classification/Pytorch/species_results.xlsx). We assumed the limited training data was the key reason for the low prediction accuracy.

Status: Updated by Calivn on 2/13/2021. I changed the batch size to 100 and 50 (included all training data in one batch) for each model. Inception-V3 can achieve 93% and 97% accuracy this time (see results from ./Models/Classification/Pytorch/species_results.xlsx). One concern is the training process of the second model is not stable (I trained 5 times with exactly same settings, 3 achieved almost 100% val acc and 2 only achieved 77%). I assumed this is due to the initialization of weights. Since we only have limited training data, once the random initialization of weights are not optimal, the training process cannot escape from the local minimum. 

Status: Started by Calvin on 2/20/2021. We will start to develop a species classfication model to classify species ignoring countries.

Status: Completed by Calvin and Jingjing on 03/06/2021. We completed the species classfication model to classify species ignoring countries. However the model is not performing well on Lane Snapper. After we compared with the results of species classfication models on country level, we found the potential reason was Lane Snapper were only from one country but other two species were from both Belize and Honduras.

##### Story 1.5.4 

Using the bounding boxes from the object detection model, crop the fish images.
Using these images to to train the model may help in improving the accuracy of the models.

Status: In progress <Abhiraj Vinnakota on 7th-Feb-2021>
Comments: I didn't try to make progress on this as, I realized that I may need to resize images before I crop. Now that I have resized the images, I need to detect fishes on these images and then crop them, which is pending.

#### Theme 1.6 
Create a dashboard that can annotate fishing activity by locations. This could be a heat map of fishing pressure across the region for individual fishers and different fishing communities.

#### Theme 1.7
Produce tool has the potential to be implemented in working fisheries which can collect data in real time to enable fisheries managers to combine fishing location with other catch data being collected.

##### Story 1.7.1
Get familiar with different tools to build a web app, such as GCP flask and AWS instance, and figure out which one is more suitable for our project.

Status: Started by Jingjing Shi and Calvin Dong on Jan 24th, 2021.

Status: Completed the deployment plan by Calvin Dong on Jan 30th, 2021. Partially completed codes for backend (by Calvin Dong) but still needed to get familiar with torchserver.

	   Completed the exploration of GCP and AWS by Jingjing Shi on Jan 30th, 2021. Partially completed codes for HTML building and cloud deployment by Jingjing Shi.

Status: Updated by Calvin on 2/13/2021. The development of backend was completed and tested on local host.

##### Story 1.7.2
Get a full list of functions needed on the app from the clients on the Feb. 1st meeting.

Status: Completed by Jingjing Shi on Feb 1st, 2021.

##### Story 1.7.3

Start to build a web app using cloud tools.

Status: Started on Jan 30th, 2021.

	  Frontend and app configuration was completed by Jingjing Shi on Feb 13th, 2021.
		
Status: Completed on 2/20/2021 by Calvin and Jingjing. The initial version of the app is completed. We may need to integrate more functions/models and improve the UI.

#### Story 1.7.4
Create a hierarchical model architecture for the development. (fish/not fish --> species? --> country? --> fishing grounds?). The plan was to use Object Detection models to detect fish (fish/not fish) and use classfication models for the last three phases.

Status: Partially completed by Calvin on 03/06/2021. The integration of classification models were completed (species? --> country? --> fishing grounds?). I have worked with Abhiraj to explore how to integrate the Object Detection models.

Status: Completed by Calvin on 03/20/2021. The integration of Object Detection model is completed.

#### Theme 1.8
Final deliverable of the project

###### Story 1.8.1
Check with the clients about the updated requirements of the projects. Wrap up codes, logs and models of the last semester. Revise the white paper from last semester.

Status: Started by Jingjing Shi and Cavin Dong on Jan 24th, 2021.
Status: All codes, models and logs are checked and maintained by Calvin Dong on Jan 30th, 2021.
            All suggestions from Yuqiang were considered, and the paper was updated both locally (on gitlab) and on cloud (google doc) by Jingjing Shi on Jan 30th, 2021.

###### Story 1.8.2

Fix overlapping timeline of image classification models and clear logic in our deliverables.

Status: Started by Jingjing Shi on Feb. 20th, 2021.

#### Theme 1.9
Classification Models for White Grunt

#### Story 1.9.1
Follow the same workflow of developing classfication models for Yellow Snapper

Status: Started by Calvin on 2/20/2021. We will start to explore the possibility of developing models for White Grunt because this species has very limited data. 

Status: Completed by Calvin and Jingjing on 3/6/2021. We completed the country classification model for White Grunt. After our exploration, we found it was difficult to develop a fising ground classification model for White Grunt due to too small training set.










