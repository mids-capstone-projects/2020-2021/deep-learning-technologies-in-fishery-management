import os

import logging
import torch
import torch.nn.functional as F
import torch.nn as nn
import numpy as np
import io
from PIL import Image
from torchvision import models, transforms

from sys import platform
from od.models import *  # set ONNX_EXPORT in models.py
from od.utils.datasets import *
from od.utils.utils import *
from od.utils.resize import *

from flask import Flask, render_template, request, redirect

app = Flask(__name__)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

th_od = 0.2
th = 0.7

#Object Detection Model
cfg = './od/yolov3-custom_run_fish.cfg'
weights = './od/weights/best_run_fish_coco.pt'
img_size = 512

model_od = Darknet(cfg, img_size)
model_od.load_state_dict(torch.load(weights, map_location=device)['model'])
model_od.eval()

#Species Model
model_sp = models.inception_v3(pretrained=False, init_weights=True).to(device)
model_sp.load_state_dict(torch.load('./cl/model_sp_all_incep.pt', map_location=torch.device('cpu')))
model_sp.eval()

#Country Model for Yellow Snapper
model_ys_ct = models.resnet34(pretrained=False).to(device)
model_ys_ct.load_state_dict(torch.load('./cl/model_ct_rgb.pt', map_location=torch.device('cpu')))
model_ys_ct.eval()

#Country Model for White Grunt
model_wg_ct = models.resnet34(pretrained=False).to(device)
model_wg_ct.load_state_dict(torch.load('./cl/model_wg_ct.pt', map_location=torch.device('cpu')))
model_wg_ct.eval()

#Fishing Ground Model for Yellow Snapper, Honduras
model_fg = models.inception_v3(pretrained=False, init_weights=True).to(device)
model_fg.load_state_dict(torch.load('./cl/model_fg_rgb_incep_hon_2.pt', map_location=torch.device('cpu')))
model_fg.eval()

#Input Transformation

transform_res = transforms.Compose([
			transforms.Resize((100,150)),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    	])

transform_incep = transforms.Compose([
			transforms.Resize((299,299)),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    	])

#Label Maps
m_fish = {'0.0':'Not Fish', '1.0':'Fish'}
m_species = {'-1.0':'NA', '0.0':'Lane Snapper', '1.0':'White Grunt', '2.0':'Yellow Snapper'}
m_country = {'-1.0':'NA', '0.0':'Belize', '1.0':'Honduras'}
m_fg = {'-1.0':'NA', '0.0':'Banco Tino', '1.0':'Seco en Medio','2.0':'Jack Bank, Harvest Cay and Grunt Patch'}


#Input preprocess


def preprocess_one_image_od(img, new_shape=img_size):
	img = cv2.imdecode(np.fromstring(img, np.uint8), cv2.IMREAD_UNCHANGED)
	img = letterbox(img, new_shape=img_size)[0]

	# Convert
	img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
	img = np.ascontiguousarray(img)

	img = torch.from_numpy(img).float().to(device)
	#img = img.half() if half else img.float()  # uint8 to fp16/32

	img /= 255.0  # 0 - 255 to 0.0 - 1.0
	if img.ndimension() == 3:
		img = img.unsqueeze(0)

	return img

def preprocess_one_image_res(img):
	img = Image.open(io.BytesIO(img))
	img = transform_res(img).unsqueeze(0)
	return img

def preprocess_one_image_incep(img):
	img = Image.open(io.BytesIO(img))
	img = transform_incep(img).unsqueeze(0)
	return img

def preprocess_od(imgs):
	imgs = [preprocess_one_image_od(img) for img in imgs]
	#imgs = torch.cat(imgs)
	return imgs

def preprocess_res(imgs):
	imgs = [preprocess_one_image_res(img) for img in imgs]
	imgs = torch.cat(imgs)
	return imgs

def preprocess_incep(imgs):
	imgs = [preprocess_one_image_incep(img) for img in imgs]
	imgs = torch.cat(imgs)
	return imgs

#Inference
def inference(imgs, country, fishing_ground):

	x_od = preprocess_od(imgs)
	x_res = preprocess_res(imgs).to(device)
	x_incep = preprocess_incep(imgs).to(device)

	out_score = {'Fish Detection':[], 
	'Classification of Species':[], 
	'Classification of Country (Yellow Snapper)':[], 
	'Classification of Country (White Grunt)':[], 
	'Classification of Fishing Ground (Yellow Snapper)':[]}
	out_result = np.zeros((len(x_res),5))
	#Initialize outputs
	out_result[:,:] = -1

	#Indexing outputs
	img_id = np.array(range(len(x_res)))
	out_result[:,0] = img_id

	pred_fish = []
	pred_ys = []
	pred_wg = []
	pred_hon = []

	#OD model#

	od_result = []
	for j,i in enumerate(x_od):
		out_od = model_od(i, augment=False, verbose=True)[0]
		out_od = non_max_suppression(out_od, 0.1, 0.6, multi_label=False, classes=None, agnostic=False)

		for obj in out_od:
			max_conf = 0
			if obj is None:
				od_result.append(max_conf)
				out_score['Fish Detection'].append({
					'id': img_id[j],
					'confidence': round(max_conf,2)
					})
			else:
				if obj.detach().numpy()[0][4] > max_conf:
					max_conf = obj.detach().numpy()[0][4].item()
				od_result.append(max_conf)
				out_score['Fish Detection'].append({
					'id': img_id[j],
					'confidence': round(max_conf,2)
					})

			if max_conf != 0:
				pred_fish.append(j)

	out_od = np.round(np.array(od_result),2)
	out_od = np.array([1 if x > 0 else 0 for x in out_od])
	out_result[:,1] = out_od

	x_incep = x_incep[pred_fish]
	x_res = x_res[pred_fish]
	img_id_fish = img_id[pred_fish]

	#Species Classification#

	if len(x_incep) > 0:

		out_sp = model_sp(x_incep)
		_, y_hat = out_sp.max(1)

		for i,j in enumerate(y_hat):
			out_score['Classification of Species'].append({
			'id':img_id_fish[i],
			'Lane snapper':np.round(np.array(F.softmax(out_sp[i])[0].cpu().item()),2),
			'White grunt':np.round(np.array(F.softmax(out_sp[i])[1].cpu().item()),2),
			'Yellow snapper':np.round(np.array(F.softmax(out_sp[i])[2].cpu().item()),2)
			})

			if j == 1:
				pred_wg.append(i)

			if j == 2:
				pred_ys.append(i)

		out_result[out_result[:,1]!=0,2] = y_hat

	#Yellow snapper: Country Classification#

	x_res_ys = x_res[pred_ys].to(device)
	x_incep_ys = x_incep[pred_ys].to(device)
	img_id_ys = img_id_fish[pred_ys]

	if len(x_res_ys) > 0:
		out_ct = model_ys_ct(x_res_ys)
		_, y_hat = out_ct.max(1)

		for i,j in enumerate(y_hat):
			out_score['Classification of Country (Yellow Snapper)'].append({
			'id':img_id_ys[i], 
			'Belize':np.round(np.array(F.softmax(out_ct[i])[0].cpu().item()),2),
			'Honduras':np.round(np.array(F.softmax(out_ct[i])[1].cpu().item()),2)
			})

			if j == 1:
				pred_hon.append(i)

		out_result[out_result[:,2]==2,3] = y_hat

	#White Grunt: Country Classification#

	x_res_wg = x_res[pred_wg].to(device)
	x_incep_wg = x_incep[pred_wg].to(device)
	img_id_wg = img_id_fish[pred_wg]

	if len(x_res_wg) > 0:
		out_ct = model_wg_ct(x_res_wg)
		_, y_hat = out_ct.max(1)

		for i,j in enumerate(y_hat):
			out_score['Classification of Country (White Grunt)'].append({
			'id':img_id_wg[i], 
			'Belize':np.round(np.array(F.softmax(out_ct[i])[0].cpu().item()),2),
			'Honduras':np.round(np.array(F.softmax(out_ct[i])[1].cpu().item()),2)
			})

			# if j == 1:
			# 	pred_hon.append(i)

		out_result[out_result[:,2]==1,3] = y_hat

	#Yellow snapper, Honduras: Fishing Ground Classification#

	x_incep_ys = x_incep_ys[pred_hon].to(device)
	img_id_ys_hon = img_id_ys[pred_hon]

	if len(x_incep_ys) > 0:
		out_fg = model_fg(x_incep_ys)
		_, y_hat = out_fg.max(1)
		for i,j in enumerate(y_hat):
			out_score['Classification of Fishing Ground (Yellow Snapper)'].append({
			'id':img_id_ys_hon[i], 
			'Banco Tino':np.round(np.array(F.softmax(out_fg[i])[0].cpu().item()),2),
			'Seco en Medio':np.round(np.array(F.softmax(out_fg[i])[1].cpu().item()),2)
			})

		#Yellow snapper, Honduras
		out_result[np.multiply(out_result[:,3]==1, out_result[:,2]==2), 4] = y_hat

	#All lane snappers from Belize
	#out_result[out_result[:,2]==0,3] = 0
	#Yellow snapper, Belize, fishing ground
	out_result[np.multiply(out_result[:,3]==0, out_result[:,2]==2), 4] = 2
	#Lane snapper, Belize, fishing ground
	out_result[np.multiply(out_result[:,3]==0, out_result[:,2]==0), 4] = 2

	#mapping labels
	out_result = np.array(out_result).astype('str')

	out_result[:,1] = [m_fish[x] for x in out_result[:,1]]

	out_result[:,2] = [m_species[x] for x in out_result[:,2]]
	out_result[:,3] = [m_country[x] for x in out_result[:,3]]
	out_result[:,4] = [m_fg[x] for x in out_result[:,4]]

	#out_result[:,1] = out_od
	out_result[:,0] = np.array(range(len(x_od)),dtype=str)

	#Labeling thresholds
	if len(out_score['Fish Detection']) > 0:
		for x in out_score['Fish Detection']:
			out_result[x['id'],1] = out_result[x['id'],1] + '*' if x['confidence'] < th_od and x['confidence'] > 0 else out_result[x['id'],1] 
	
	if len(out_score['Classification of Species']) > 0:
		for x in out_score['Classification of Species']:
			out_result[x['id'],2] = out_result[x['id'],2] + '*' if max([x['Yellow snapper'], x['White grunt'], x['Lane snapper']]) < th else out_result[x['id'],2] 
	
	if len(out_score['Classification of Country (Yellow Snapper)']) > 0:				
		for x in out_score['Classification of Country (Yellow Snapper)']:
			out_result[x['id'],3] = out_result[x['id'],3] + '*' if max([x['Belize'], x['Honduras']]) < th else out_result[x['id'],3]

	if len(out_score['Classification of Country (White Grunt)']) > 0:
		for x in out_score['Classification of Country (White Grunt)']:
			out_result[x['id'],3] = out_result[x['id'],3] + '*' if max([x['Belize'], x['Honduras']]) < th else out_result[x['id'],3]
	
	if len(out_score['Classification of Fishing Ground (Yellow Snapper)']):	
		for x in out_score['Classification of Fishing Ground (Yellow Snapper)']:

			out_result[x['id'],4] = out_result[x['id'],4] + '*' if max([x['Banco Tino'], x['Seco en Medio']]) < th else out_result[x['id'],4]			
		
	return out_score, out_result

@app.route('/predict', methods=['GET', 'POST'])
def upload_file():
	if request.method == 'POST':
		if 'file' not in request.files:
			return redirect(request.url)
        # if not file:
        # 	return

		files = request.files.getlist('file')
		imgs = [file.read() for file in files]

		score, predict = inference(imgs, True, True)

		return render_template('result2.html', Fish=score['Fish Detection']
								,Species=score['Classification of Species']
								,Country_ys=score['Classification of Country (Yellow Snapper)']
								,Country_wg=score['Classification of Country (White Grunt)']
                               ,Fishing_ground=score['Classification of Fishing Ground (Yellow Snapper)']
                               ,Final_result=predict)
	return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True, port=int(os.environ.get('PORT', 5000)))

